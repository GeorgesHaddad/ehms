from django.contrib import admin
from django.urls import path, include

app_name = "EHMSAPP"

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("EHMSAPP.urls")),
]
