from django.apps import AppConfig


class EhmsappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'EHMSAPP'
