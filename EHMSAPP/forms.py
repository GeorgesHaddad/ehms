from django import forms
from django.contrib.auth.forms import UserCreationForm

from EHMSAPP.models import Patient, Doctor, Appointment, Prescription

GENDER_CHOICES = (
    ("Male", "Male"),
    ("Female", "Female"),
    ("Other", "Other"),
    ("Prefer not to say", "Prefer not to say"),
)
SPECIALIZATION_CHOICES = (
    ("Dermatology", "Dermatology"),
    ("Family medicine", "Family medicine"),
    ("Anesthesiology", "Anesthesiology"),
    ("Radiology", "Radiology"),
    ("Emergency medicine", "Emergency medicine"),
    ("Ophtalmology", "Ophtalmology"),
)


class DoctorFilterForm(forms.Form):
    specialization = forms.ChoiceField(choices=(("All", "All"),) + SPECIALIZATION_CHOICES)
    fields = [
        "specialization"
    ]


class PatientForm(UserCreationForm):
    gender = forms.ChoiceField(choices=GENDER_CHOICES)

    class Meta:
        model = Patient
        fields = [
            "first_name",
            "last_name",
            "username",
            "email",
            "birthday",
            "photo",
            "is_doctor",
            "allergies",
            "medical_issues",
            "gender",
            "phone_number",
            "security_question"
        ]


class DoctorForm(UserCreationForm):
    gender = forms.ChoiceField(choices=GENDER_CHOICES)
    specialization = forms.ChoiceField(choices=SPECIALIZATION_CHOICES)

    class Meta:
        model = Doctor
        fields = [
            "first_name",
            "last_name",
            "username",
            "specialization",
            "email",
            "birthday",
            "phone_number",
            "gender",
            "city",
            "address",
            "photo",
            "is_doctor",
            "security_question"
        ]


class AddAppointmentForm(forms.ModelForm):
    class Meta:
        model = Appointment
        doctor = forms.ChoiceField(disabled=True)
        fields = [
            "doctor",
            "patient",
            "date",
            "time",
            "meeting_link",
        ]


class AddPrescriptionForm(forms.ModelForm):
    class Meta:
        model = Prescription
        fields = [
            "doctor",
            "patient",
            "date",
            "description",
        ]


class ForgotPasswordForm(forms.Form):
    username = forms.CharField()
    security_question = forms.CharField()
    fields = [
        username,
        security_question,
    ]
