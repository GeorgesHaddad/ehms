from django.urls import path

from EHMSAPP import views
from django.conf import settings
from django.conf.urls.static import static

app_name = "EHMSAPP"

urlpatterns = [
    path("", views.home),
    path("appointments", views.supervisor_appointments),
    path("patients", views.supervisor_patients),
    path("prescriptions", views.supervisor_prescriptions),
    path("login/", views.login_request),
    path("doctors/", views.get_all_doctors),
    path("logout/", views.logout_request),
    path("register/doctor", views.register_doctor),
    path("register/patient", views.register_patient),
    path("doctor/<str:username>", views.doctor_profile, name="doctor_profile"),
    path("patient/<str:username>", views.patient_profile, name="patient_profile"),
    path("patient/<str:username>/edit", views.edit_profile, name="edit_profile"),
    path("patient/<str:username>/delete", views.delete_profile, name="delete_profile"),
    path("doctor/<str:username>/edit", views.edit_doc, name="edit_doc"),
    path("doctor/<str:username>/delete", views.delete_doc, name="delete_doc"),
    path("doctor/<str:username>/addappointment", views.add_appointment, name="add_appointment"),
    path("doctor/<str:username>/appointments/<str:month>", views.CalendarView.as_view(), name="calendar"),
    path("patient/<str:username>/appointments", views.patient_appointments),
    path("patient/<str:username>/appointments/<int:appointment_id>/pay", views.pay_for_appointment),
    path("patient/<str:username>/appointments/<int:appointment_id>/edit", views.edit_appointment, name="edit_appointment"),
    path("patient/<str:username>/appointments/<int:appointment_id>/delete", views.delete_appointment, name="delete_appointment"),
    path("patient/<str:username>/addappointment", views.add_appointment, name="add_appointment"),
    path("forgot_password", views.forgot_password),
    path("patient/<str:username>/prescriptions", views.patient_prescriptions),
    path("patient/<str:username>/prescriptions/add", views.add_prescriptions),
    path("patient/<str:username>/prescriptions/<int:prescription_id>/delete", views.delete_prescriptions, name="delete_prescription"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
