from django.contrib.auth.models import AbstractUser
from django.db import models

optional = {"null": True, "blank": True, "default": None}
required = {"null": False, "blank": False}


class User(AbstractUser):
    username = models.CharField(max_length=127, unique=True)
    first_name = models.CharField(max_length=127, **required)
    last_name = models.CharField(max_length=127, **required)
    birthday = models.DateField(**required)
    photo = models.ImageField(**optional)
    phone_number = models.CharField(max_length=30, unique=True)
    gender = models.CharField(max_length=127, **required)
    email = models.EmailField(**required)
    is_doctor = models.BooleanField(**required)
    security_question = models.CharField(max_length=200, **optional)
    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["birthday", "is_doctor", "gender", "first_name", "last_name", "email"]


class Doctor(User):
    class Meta:
        verbose_name = "Doctor"

    city = models.CharField(max_length=127, **required)
    address = models.CharField(max_length=127, **optional)
    specialization = models.CharField(max_length=127, **required)

    def __str__(self):
        return self.username


class Patient(User):
    class Meta:
        verbose_name = "Patient"

    allergies = models.TextField(**optional)
    medical_issues = models.TextField(**optional)

    def __str__(self):
        return self.username


class Appointment(models.Model):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, **required)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE, **required)
    date = models.DateField(**required)
    time = models.TimeField(**required)
    meeting_link = models.CharField(max_length=400, **required)

    def __str__(self):
        return f"{self.patient.username} {self.doctor.username}"


class Prescription(models.Model):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, **required)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE, **required)
    date = models.DateField(**required)
    description = models.TextField(**required)

    def __str__(self):
        return f"{self.patient.username} {self.doctor.username}"
