import calendar

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.hashers import make_password
from django.shortcuts import render, redirect
from EHMSAPP.forms import *
from datetime import datetime, timedelta, date
from calendar import HTMLCalendar
from django.views import generic
from django.utils.safestring import mark_safe

from .models import *


class Calendar(HTMLCalendar):
    def __init__(self, year=None, month=None, username=None):
        self.withyear = True
        self.username = username
        self.year = year
        self.month = month
        super(Calendar, self).__init__()

    # formats a day as a td
    # filter events by day
    def formatday(self, day, events):
        events_per_day = events.filter(date__day=day)
        d = ''
        for event in events_per_day:
            d += f'<li> <u>{event.time.hour}h{event.time.minute}m:</u> <strong>{event.patient}</strong><br>link:<a href="//{event.meeting_link}"> {event.meeting_link} </a></li>'

        if day != 0:
            return f"<td><span class='date'>{day}</span><ul> {d} </ul></td>"
        return '<td></td>'

    # formats a week as a tr
    def formatweek(self, **kwargs):
        week_days = kwargs.get("week")
        events = kwargs.get("events")
        week = ''
        for d, weekday in week_days:
            week += self.formatday(d, events)
        return f'<tr> {week} </tr>'

    # formats a month as a table
    # filter events by year and month
    def formatmonth(self, **kwargs):
        events = Appointment.objects.filter(date__year=self.year, date__month=self.month,
                                            doctor__username=self.username)

        cal = f'<table border="0" cellpadding="0" cellspacing="0" class="calendar">\n'
        cal += f'{self.formatmonthname(self.year, self.month, withyear=self.withyear)}\n'
        cal += f'{self.formatweekheader()}\n'
        for week in self.monthdays2calendar(self.year, self.month):
            cal += f'{self.formatweek(week=week, events=events)}\n'
        return cal


class CalendarView(generic.ListView):
    model = Appointment
    template_name = 'doctors/appointments.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # use today's date for the calendar
        # d = get_date(self.request.GET.get('day', None))
        d = get_date(self.kwargs['month'])

        # Instantiate our calendar class with today's year and date
        cal = Calendar(d.year, d.month, self.kwargs['username'])

        # Call the formatmonth method, which returns our calendar as a table
        html_cal = cal.formatmonth(withyear=True)
        context['calendar'] = mark_safe(html_cal)
        context['username'] = self.kwargs['username']
        context['prev_month'] = prev_month(d)
        context['next_month'] = next_month(d)
        return context


def prev_month(d):
    first = d.replace(day=1)
    previous_month = first - timedelta(days=1)
    month = str(previous_month.year) + '-' + str(previous_month.month)
    return month


def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + timedelta(days=1)
    month = str(next_month.year) + '-' + str(next_month.month)
    return month


def get_date(req_day):
    if req_day:
        year, month = (int(x) for x in req_day.split('-'))
        return date(year, month, day=1)
    return datetime.today()


def home(request):
    supervisor = request.user.is_authenticated and request.user.is_superuser
    return render(request=request, template_name="home.html", context={"supervisor": supervisor})


def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            credentials = form.cleaned_data
            username = credentials["username"]
            password = credentials["password"]
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                member = request.user
                if member.is_superuser:
                    return redirect("/")
                elif member.is_doctor:
                    return redirect(f"/doctor/{username}")
                else:
                    return redirect(f"/patient/{username}")
            else:
                messages.warning(request, "Invalid Username or Password")
        else:
            messages.warning(request, "Invalid Username or Password")
    form = AuthenticationForm()
    return render(request=request, template_name="login_page.html", context={"form": form})


def logout_request(request):
    logout(request)
    return redirect("/login/")


def register(request, form):
    if request.method == "POST":
        form = form
        if form.is_valid():
            form.save()
            return redirect("/login/")
        else:
            print(form.errors)
            return render(
                request=request,
                template_name="register_page.html",
                context={"form": form},
            )
    else:
        form = form
        return render(request, "register_page.html", context={"form": form})


def register_patient(request):
    return register(request, PatientForm(request.POST, request.FILES))


def register_doctor(request):
    return register(request, DoctorForm(request.POST, request.FILES))


def doctor_profile(request, username):
    year = str(datetime.now().year)
    month = str(datetime.now().month)
    date = year + '-' + month
    doctor = Doctor.objects.get(username=username)
    is_doctor = request.user.is_authenticated and request.user.is_doctor
    supervisor = request.user.is_authenticated and request.user.is_superuser
    flag = request.user.username == username or supervisor
    return render(request, "doctors/profile.html",
                  context={"doctor": doctor, "is_doctor": is_doctor, "month": date, "flag": flag,
                           "supervisor": supervisor})


def patient_profile(request, username):
    patient = Patient.objects.get(username=username)
    supervisor = request.user.is_authenticated and request.user.is_superuser
    return render(request, "patients/profile.html", context={"patient": patient, "supervisor": supervisor})


def add_appointment(request, username):
    if request.method == "POST":
        form = AddAppointmentForm(request.POST)
        form.doctor = username
        if form.is_valid():
            form.save()
            if request.user.is_superuser:
                return redirect("/appointments")
            return redirect(f"/patient/{request.user.username}/appointments")
    form = AddAppointmentForm()
    supervisor = request.user.is_authenticated and request.user.is_superuser
    return render(request, "patients/add_appointment.html",
                  {"form": form, "username": username, "supervisor": supervisor})


def get_all_doctors(request):
    if request.method == "POST":
        form = DoctorFilterForm(request.POST)
        specialization = form.data["specialization"]
        if specialization == "All":
            doctors = Doctor.objects.all()
        else:
            doctors = Doctor.objects.filter(specialization=specialization)
    else:
        doctors = Doctor.objects.all()
    form = DoctorFilterForm()
    supervisor = request.user.is_authenticated and request.user.is_superuser
    return render(request, "all_doctors.html",
                  {"doctors": doctors, "supervisor": supervisor, "form": form})


def forgot_password(request):
    if request.method == "POST":
        form = ForgotPasswordForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            user = User.objects.get(username=username)
            if form.cleaned_data["security_question"] == user.security_question:
                user.password = make_password("default")
                user.save()
                changed = True
                return render(request, "forgot_password.html", {"form": form, "changed": changed})
    changed = False
    form = ForgotPasswordForm()
    return render(request, "forgot_password.html", {"form": form, "changed": changed})


def delete_profile(request, username):
    Patient.objects.get(username=username).delete()
    if request.user.is_superuser:
        return redirect("/patients")
    logout(request)
    return redirect("/")


def edit_profile(request, username):
    patient = Patient.objects.get(username=username)
    if request.method == "POST":
        form = PatientForm(request.POST, request.FILES, instance=patient)
        if form.is_valid():
            form.save()
            username = form.cleaned_data["username"]
            if request.user.is_superuser:
                return redirect("/patients")
            user = authenticate(username=username, password=form.cleaned_data["password1"])
            if user is not None:
                login(request, user)
            return redirect(f"/patient/{username}")
        else:
            return render(request, "patients/edit_profile.html", {"form": form})
    else:
        form = PatientForm(instance=patient)
        supervisor = request.user.is_authenticated and request.user.is_superuser
        return render(request, "patients/edit_profile.html", {"form": form, "supervisor": supervisor})


def doctor_appointments(request, username):
    doctor = Doctor.objects.get(username=username)
    appointments = Appointment.objects.filter(doctor=doctor)
    return render(request, "doctors/appointments.html", context={"appointments": appointments})


def patient_appointments(request, username):
    patient = Patient.objects.get(username=username)
    appointments = Appointment.objects.filter(patient=patient)
    appointments = appointments.order_by('date').order_by('time')
    return render(request, "patients/appointments.html", context={"appointments": appointments})


def pay_for_appointment(request, username, appointment_id):
    appointment = Appointment.objects.get(id=appointment_id)
    return render(request, "patients/pay_for_appointment.html", context={"appointment": appointment})


def delete_appointment(request, username, appointment_id):
    Appointment.objects.get(id=appointment_id).delete()
    if request.user.is_superuser:
        return redirect("/appointments")
    return redirect(f"/patient/{username}/prescriptions/add")


def edit_appointment(request, username, appointment_id):
    appointment = Appointment.objects.get(id=appointment_id)
    if request.method == "POST":
        form = AddAppointmentForm(request.POST, request.FILES, instance=appointment)
        if form.is_valid():
            form.save()
            if request.user.is_superuser:
                return redirect("/appointments")
            return redirect(f"/patient/{username}/appointments")
    form = AddAppointmentForm(instance=appointment)
    supervisor = request.user.is_authenticated and request.user.is_superuser
    return render(request, "patients/edit_appointment.html", {"form": form, "supervisor": supervisor})


def edit_doc(request, username):
    doctor = Doctor.objects.get(username=username)
    if request.method == "POST":
        form = DoctorForm(request.POST, request.FILES, instance=doctor)
        if form.is_valid():
            form.save()
            username = form.cleaned_data["username"]
            user = authenticate(username=username, password=form.cleaned_data["password1"])
            if user is not None:
                login(request, user)
            return redirect(f"/doctor/{username}")
        else:
            return render(request, "doctors/edit_doc.html", {"form": form})
    else:
        form = DoctorForm(instance=doctor)
        supervisor = request.user.is_authenticated and request.user.is_superuser
        return render(request, "doctors/edit_doc.html", {"form": form, "supervisor": supervisor})


def delete_doc(request, username):
    Doctor.objects.get(username=username).delete()
    if request.user.is_superuser:
        return redirect("/doctors")
    logout(request)
    return redirect("/")


def supervisor_appointments(request):
    appointments = Appointment.objects.all()
    return render(request, "supervisor/appointments.html", context={"appointments": appointments})


def supervisor_patients(request):
    patients = Patient.objects.all()
    return render(request, "supervisor/patients.html", context={"patients": patients})


def supervisor_prescriptions(request):
    prescriptions = Prescription.objects.all()
    return render(request, "supervisor/prescriptions.html", context={"prescriptions": prescriptions})


def patient_prescriptions(request, username):
    patient = Patient.objects.get(username=username)
    prescriptions = Prescription.objects.filter(patient=patient)
    return render(request, "patients/prescriptions.html", context={"prescriptions": prescriptions})


def add_prescriptions(request, username):
    if request.method == "POST":
        form = AddPrescriptionForm(request.POST)
        if form.is_valid():
            form.save()
            if request.user.is_superuser:
                return redirect("/prescriptions")
            return redirect(f"/patient/{request.user.username}/prescriptions")
    form = AddPrescriptionForm()
    supervisor = request.user.is_authenticated and request.user.is_superuser
    return render(request, "patients/add_prescriptions.html",
                  {"form": form, "username": username, "supervisor": supervisor})


def delete_prescriptions(request, username, prescription_id):
    Prescription.objects.get(id=prescription_id).delete()
    if request.user.is_superuser:
        return redirect("/prescriptions")
    return redirect(f"/patient/{username}/prescriptions")
